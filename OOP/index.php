<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    // echo "Jenis hewan : " . $sheep ->type;
    // echo "<br>";
    echo  "Name : " . $sheep->name;
    echo "<br>";
    echo  "legs : " . $sheep->legs;
    echo "<br>";
    echo  "cold_blooded : " . $sheep->cold_blooded;
    echo "<br>";

    $kodok = new Frog("buduk");
    echo "<br>";
    echo  "Name : " . $kodok->name;
    echo "<br>";
    echo  "legs : " . $kodok->legs;
    echo "<br>";
    echo  "cold_blooded : " . $kodok->cold_blooded;
    echo "<br>";
    echo  "Jump : " . $kodok->jump;
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "<br>";
    echo  "Name : " . $sungokong->name;
    echo "<br>";
    echo  "legs : " . $sungokong->legs;
    echo "<br>";
    echo  "cold_blooded : " . $sungokong->cold_blooded;
    echo "<br>";
    echo  "Yell : " . $sungokong->yell;
    echo "<br>";
    // echo $sungokong->warna("Cokelat");



?>